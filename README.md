# Hugo Template Project

This project serves as a template for quickly creating websites with Hugo,
integrating modern development tools to ensure high code quality and ease of
maintenance.

## Prerequisites

Before you start, make sure you have installed the following software on your
machine:

- **Git**: Version control tool needed for cloning the repository and managing
  versions.
- **Hugo**: Static site generator, necessary to build websites.
- **Python 3**: Required for running pre-commit and Commitizen.
- **pip**: Python package manager, used to install tools like pre-commit and
  Commitizen.

## Installation

Follow these steps to set up the project locally.

### Clone the Project

To get the project, execute this command in your terminal:

```bash
git clone https://gitlab.com/username/hugo_template.git
cd hugo_template
```

### Set Up Python Environment

Create and activate a virtual environment, and install the dependencies:

```bash
python3 -m venv venv
source venv/bin/activate
pip install -r requirements.txt
```

### Install pre-commit and Commitizen

Set up pre-commit and Commitizen in your local repository:

```bash
pre-commit install
cz init
```

## Usage

To start the Hugo server and work on your site:

```bash
hugo server
```

To make a commit with Commitizen, use:

```bash
cz commit
```

## Generate New Projects with Copier

To create a new project based on this template, ensure Copier is installed and
execute:

```bash
copier path/to/this_project path/to/new_project
```

Follow the interactive instructions to customize your new Hugo project.

## Project Structure

Explain here how your project is organized, for example:

- **content/**: Contains the site content, such as blog posts.
- **themes/**: Contains the Hugo themes used.
- **config.toml**: Configuration file for Hugo.

## Docker container
If container running in different architecture like arm64 for macbook or amd64 for linux system build the image with --build-arg option

```bash
docker build . -t <image_name> --build-arg ARCH=<my_architecture>
```
And after that run your image in a container

```bash
docker run -d --rm --name <container_name> -p 80:80 <name_image> 
```

## Contributing

Encourage others to contribute to the project by explaining how they can submit
pull requests or issues.

## License

```plaintext
This project is licensed under the MIT License - see the LICENSE.md file for
details.
```
