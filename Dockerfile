# Dockerfile.hugo
FROM debian:12.5-slim as builder

ARG ARCH=amd64

RUN apt update && apt install -y wget dpkg \
    && wget -O hugo.deb https://github.com/gohugoio/hugo/releases/download/v0.126.3/hugo_extended_0.126.3_linux-${ARCH}.deb \ 
    && dpkg -i hugo.deb


WORKDIR /src

# Copy the Hugo site files to the container
COPY . .

# Build the Hugo site
RUN hugo 

# Stage 2: Nginx
FROM nginx:alpine

# Copy the generated Hugo site to Nginx's html directory
COPY --from=builder --chown=nginx:nginx /src/public /usr/share/nginx/html

# Expose port 80
EXPOSE 80
